<?php

namespace App\Http\Controllers\General;

use App\Models\General\Order;
use App\Models\General\Product;
use GuzzleHttp\Exception\ClientException;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;

class SiteController extends Controller
{
    private $client_code = '07ae71d4';
    private $client_api_key = '44d8532a1819fd1c004811aab3b2f4048fb200c1a621114bcc73e67432badc9e';
    private $root_api_key = '92cb816ef155a56b36f39d6dd88a8448fdf57bb2e77fc40096bebb6b568c2851';

    public function index()
    {
        $products = Product::all();

        return view('site.index', [
            'products' => $products
        ]);
    }

    public function createDusoftQuoteForOrder(Request $request)
    {
        $product_id = $request->input('product_id');

        $order = new Order();
        $order->product_id = $product_id;
        $order->save();

        $product = Product::find($product_id);

        $product_amount = round($product->amount, 2);
        $item_url = 'http://milano.tj/product/'.$product_id;

        $hash = $this->sha256Hash($order->id.$product_amount.$this->client_api_key);

        /** @var \GuzzleHttp\Psr7\Response $response */
        $response = new \GuzzleHttp\Psr7\Response();

        $client = new \GuzzleHttp\Client();

        $requestMethod = 'POST';
        $url = 'http://upwork.tj/api/pay/createQuote';

        try {
            $response = $client->request($requestMethod, $url, [
                'form_params' => [
                    'client_code' => $this->client_code,
                    'hash' => $hash,
                    'order_id' => $order->id,
                    'amount' => $product_amount,
                    'item_url' => $item_url,
                ]
            ]);
        } catch (ClientException $ex) {
            echo 'hey client';
            $resEx = $ex->getMessage();

            return $resEx;
        } catch (ConnectException $ex) {
            $resEx = $ex->getMessage();

            return $resEx;
        } catch (RequestException $ex) {
            echo 'hey request';
            $resEx = $ex->getMessage();

            return $resEx;
        }

        $response_data = json_decode($response->getBody()->getContents());

        if ($response_data->success) {
            return Redirect::away('http://upwork.tj/api/pay/gateway/'.$response_data->signature.'/'.$order->id.'/'.$this->client_code);
        }
    }

    public function sha256Hash($str)
    {
        return hash('sha256', $str);
    }
}
